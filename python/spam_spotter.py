from fake_useragent import UserAgent
import requests
import urllib.request
import socket
from spam_lists import SPAMHAUS_DBL
import hashlib
import re
import json
import pandas as pd
import string
import tldextract
import time
import datetime


LOG_DIR = './intelligence/csv/'
SITE_SEARCH_SPAM_SAMPLE = './intelligence/csv/site-search-spam.csv'
GOOGLE_SEARCH_URL = 'https://www.google.com/search?q={query}'
GOOGLE_RESULTS_DIR = './intelligence/html/google/'
NABP_PHARMACY_NAUGHTY_LIST = './intelligence/csv/naughty_list.csv'
PHONE_NUMBERS_DIR = './intelligence/phones/'
GOOGLE_REFERER = 'https://www.google.com/'
GOOGLE_BOT_USER_AGENT = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
SPAM_PAGE_KEYWORDS = r'(viagra)|(cialis[^t])|(pharm)|(pill)'
ASN_LOOKUP_URL = 'https://api.iptoasn.com/v1/as/ip/{ip}'


# def get_logger(loc=LOG_FILE):
#     return pd.read_csv(loc)


def get_sample_spam(csv=SITE_SEARCH_SPAM_SAMPLE):
    df = pd.read_csv(csv)
    printable = set(string.printable)
    df['Title'] = df['Title'].apply(lambda x: ''.join([" - " if i not in printable else i for i in x]))
    df['Desc'] = df['Desc'].apply(lambda x: ''.join([" - " if i not in printable else i for i in x]))
    return df


def get_spammy_titles(df, col='Title'):
    return df[col].values


def get_spammy_descriptions(df, col='Desc'):
    return df[col].values


def get_google_results(query, user_agent=None):  # do not abuse
    if user_agent is None:
        ua = UserAgent()
        user_agent = ua['google chrome']  # selects a random chrome user agent
    query = query.replace(' ', '+')
    url = GOOGLE_SEARCH_URL.format(query=query, headers={'User-agent': user_agent})
    response = requests.get(url)
    return response


def get_links_from_google_results(html):
    link_search = r'q=(https?://[^;&"\s]+)'
    pharma_search = r'(pharm)|(canad)|(drug)|(pill)|(viagra)|(cialis)|(health)|(vision)|(medicine)|(rx)|(care)'
    results = re.findall(link_search, html, re.MULTILINE)
    results_to_keep = {}
    for e, result in enumerate(results):
        domain_info = tldextract.extract(result)
        if (domain_info.domain not in ['google', 'googleusercontent']) \
                and (domain_info.suffix not in ['gov', 'pharmacy']) \
                and re.search(pharma_search, domain_info.domain) is None:
            results_to_keep[e] = result
    return results_to_keep


def google_search(query, user_agent=None):
    html = get_google_results(query, user_agent).text
    return get_links_from_google_results(html)


def check_for_cloaking(url, user_agent=None):
    results = {}
    if user_agent is None:
        ua = UserAgent()
        user_agent = ua['google chrome']  # selects a random chrome user agent
    for agent in [user_agent, GOOGLE_BOT_USER_AGENT]:
        for referer in [GOOGLE_REFERER, None]:
            response = download_page(url, agent, referrer=referer)
            agent = response.request.headers['User-agent']
            if agent == GOOGLE_BOT_USER_AGENT:
                agent_key = 'googlebot'
            else:
                agent_key = 'user'
            if referer == GOOGLE_REFERER:
                referer_key = 'google-ref'
            else:
                referer_key = 'no-ref'
            key = (agent_key, referer_key)
            text_without_comments = re.sub(r'(<!--.*?-->)', '', response.text, flags=re.MULTILINE)
            md5 = hashlib.md5(text_without_comments.encode('utf-8')).hexdigest()
            results[key] = {'md5': md5, 'response': response, 'referer': referer, 'user-agent': agent}
    return results


def cloaking_exists(results):
    hashes = []
    for k, v in results.items():
        hashes.append(v['md5'])
    if all(x == hashes[0] for x in hashes):
        return False
    else:
        return True


def check_robots_txt(domain):
    if 'http' not in domain:
        url = 'http://' + domain + '/robots.txt'
    else:
        url = domain + '/robots.txt'
    return requests.get(url).text


def check_if_robots_disabled(robots_txt):
    if robots_txt.replace('\n', '') == 'User-Agent: *Disallow: /':
        return True
    else:
        return False


def check_if_seized(html):
    keywords = ['Seized', 'seized', 'DOJ', 'Europol']
    if any(keyword in html for keyword in keywords):
        return True
    else:
        return False


def contains_spam(html):
    results = re.findall(SPAM_PAGE_KEYWORDS, html, re.MULTILINE | re.IGNORECASE)
    if results:
        return True
    else:
        return False


def check_naughty_list(domain, naughty_list=NABP_PHARMACY_NAUGHTY_LIST):
    df = pd.read_csv(naughty_list, encoding='latin1')
    return domain in df['Web Site Address'].values


def check_spam_list(domain):
    return domain in SPAMHAUS_DBL


def get_ip_from_domain(domain):
    return socket.gethostbyname(domain)


def get_asn_info(ip):
    try:
        response = requests.get(ASN_LOOKUP_URL.format(ip=ip))
        return json.loads(response.text)
    except json.JSONDecodeError:
        print ("failed to get ASN info for ip:", ip)


def parse_domain(url):
    info = tldextract.extract(url)
    return info.domain+'.'+info.suffix


def check_for_contact_url(html):
    search = r'href="([^"]+contact([^"]+)?)"'
    result = re.search(search, html, re.MULTILINE)
    if result:
        result.group(1)
    return result


def check_for_phone_numbers(html):
    results = {}
    image_phone_search = r'<img.+src="([^"]+(P|p)hone[^"]+)"'
    class_phone_search = r'class="[^"]*(P|p)hone[^"]*">'
    plain_text_search = r'((\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4})'
    results['image'] = re.search(image_phone_search, html)
    results['class'] = re.search(class_phone_search, html)
    results['plain_text'] = re.search(plain_text_search, html)

    if results['image']:
        results['image'] = results['image'].group(1)

    if results['class']:
        results['class'] = parse_phone_number(html)
    else:
        results['class'] = None

    return results


def parse_phone_number(html):
    try:
        number_mapping = {'plus': '+', 'minus': '-', 'zero': '0', 'one': '1', 'two': '2', 'three': '3', 'four': '4',
                          'five': '5', 'six': '6', 'seven': '7', 'eight': '8', 'nine': '9',
                          'i_add': '+', 'i_line': '-', 'i1': '1', 'i2': '2', 'i3': '3', 'i4': '4', 'i5': '5',
                          'i6': '6', 'i7': '7', 'i8': '8', 'i9': '9', 'i0': '0'}
        search = r'<((span)|(i)) class="((plus)|(minus)|(one)|(two)|(three)|(four)|(five)|(six)|(seven)|(eight)|(nine)|(zero)|(i_add)|(i_line)|(i1)|(i2)|(i3)|(i4)|(i5)|(i6)|(i7)|(i8)|(i9)|(i0))'
        results = re.findall(search, html)
        usa_number_digits = 15
        usa_phone_number = ''
        for x in range(usa_number_digits):
            counter = 0
            for y in results[x]:
                if (y not in ['span', '']) and (counter == 0):
                    usa_phone_number += number_mapping[y]
                    counter += 1
        return usa_phone_number
    except IndexError:
        return None


def download_image(base_url, link, loc=PHONE_NUMBERS_DIR):
    if 'http' not in link:
        link = link[2:]
        url = base_url + link
    else:
        url = link
    loc_filename = \
        loc + parse_domain(url).replace('.', '-') + '-' + 'phone.png'
    try:
        print ("getting... ", url)
        urllib.request.urlretrieve(url, loc_filename)
        return loc_filename
    except urllib.error.URLError as e:
        print (e, 'base_url:', base_url, 'link:', link)


def search_for_phone_numbers(base_url, html, results, loc=PHONE_NUMBERS_DIR, depth=0):
    results = check_for_phone_numbers(html)
    if results['image']:
        saved_to = download_image(base_url, link=results['image'], loc=loc)
        print ("phone image saved: ", saved_to)
        return
    if (results['image'] is None) and (results['plain_text'] is None) and (results['class'] is None) and (depth == 0):
        contact_url = check_for_contact_url(html)
        if contact_url:
            contact_page_html = requests.get(contact_url).text
            depth += 1
            results = search_for_phone_numbers(base_url=contact_url, html=contact_page_html, results=results, depth=depth)
            return results


def check_base_href(html):
    search = r'((<base href)|(<frame src))="([^"]+)"'
    try:
        result = re.search(search, html, re.MULTILINE).group(4)
        result = result
        return result
    except AttributeError:
        return None


def check_third_party_content(domain, html):
    base_href = check_base_href(html)
    if (base_href is not None) and (parse_domain(base_href) != domain):
        return base_href
    else:
        return None


def parse_redirect_loc(domain, html):
    search = r'location\.href="([^"]+)"'
    try:
        location = re.search(search, html).group(1)
        if location[-3:] != '.js':
            if location[0:2] == '/?':
                location = 'http://'+domain+location
    except AttributeError:
        location = None
    return location


def download_page(url, user_agent=None, referrer=None, allow_redirects=True, session=None):
    time.sleep(3)
    if session is None:
        session = requests.Session()
    if user_agent is None:
        ua = UserAgent()
        user_agent = ua['google chrome']  # selects a random chrome user agent
    headers = {'referer': referrer, 'User-agent': user_agent}
    print ("getting...", url, "\n\t referrer:", referrer, "\n\t user-agent: ", user_agent)
    response = session.get(url, headers=headers, allow_redirects=allow_redirects)
    if response is None:
        raise AttributeError('failed to get:', url)
    else:
        return response


def check_for_affiliates (response_history):
    affiliates = []
    if len(response_history) > 2:
        for response in response_history[1:-1]:
            url = response.url
            domain = parse_domain(url)
            if (domain != parse_domain(response_history[0].url)) and (domain != parse_domain(response_history[-1].url)):
                affiliates.append(url)
    return affiliates


def gather_intel_url(url, user_agent=None, referer=None, follow_links=False, depth=0):
    url = url
    referer = referer
    ts = time.time()
    if user_agent is None:
        ua = UserAgent()
        user_agent = ua['google chrome']  # selects a random chrome user agent
    page_response = download_page(url, user_agent=user_agent, referrer=referer,
                                  allow_redirects=follow_links)
    url = page_response.url
    domain = parse_domain(url)
    print ("checking ip...")
    ip = get_ip_from_domain(domain)
    print ("checking asn...")
    asn_data = get_asn_info(ip)
    html = page_response.text
    spam_found = contains_spam(html)
    seized_domain = check_if_seized(html)
    print ("checking robots.txt...")
    robots_txt = check_robots_txt(domain)
    robots_disabled = check_if_robots_disabled(robots_txt)
    on_bad_pharma_list = check_naughty_list(domain)
    on_spam_list = check_spam_list(domain)

    if depth == 0:
        print ("checking cloaking...")
        cloaking_results = check_for_cloaking(url, user_agent=user_agent)
        cloaking_found = cloaking_exists(cloaking_results)
    else:
        cloaking_found = None
        cloaking_results = None

    if cloaking_found:
        print ("cloaking found!")
        depth += 1
        cloaked_html = cloaking_results[('user', 'google-ref')]['response'].text
        third_party_base_href = check_third_party_content(domain, cloaked_html)
        if (follow_links is True) and (third_party_base_href is not None):
            print ("checking base_href links...")
            third_party_base_href_results = gather_intel_url('http://'+parse_domain(third_party_base_href),
                                                             user_agent=user_agent, referer=GOOGLE_REFERER, depth=depth)

        redirect_loc = parse_redirect_loc(domain, cloaking_results[('user', 'google-ref')]['response'].text)
        if (follow_links is True) and (redirect_loc is not None):
            print ("checking redirect links...")
            redirect_results = gather_intel_url(url=redirect_loc, user_agent=user_agent,
                                                follow_links=True, referer=page_response.url, depth=depth)

    affiliates = check_for_affiliates(page_response.history)
    if affiliates:
        affiliate_details = dict()
        for affiliate in affiliates:
            affiliate_details[affiliate] = gather_intel_url(url='http://'+parse_domain(affiliate),
                                                            user_agent=user_agent, follow_links=False, referer=page_response.url, depth=1)

    if spam_found is True:
        base_url = check_base_href(html)
        if base_url is None:
            base_url = 'http://'+domain
        phone_results = search_for_phone_numbers(base_url=base_url, html=html, loc=PHONE_NUMBERS_DIR, results=None)
    else:
        phone_results = None

    try:
        redirect_results
    except NameError:
        redirect_results = None

    try:
        third_party_base_href
    except NameError:
        third_party_base_href = None

    try:
        third_party_base_href_results
    except NameError:
        third_party_base_href_results = None

    try:
        redirect_loc
    except NameError:
        redirect_loc = None

    try:
        affiliate_details
    except NameError:
        affiliate_details = None

    return {
        'url': url, 'referer': referer,
        'timestamp': datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'),
        'domain': domain, 'ip': ip, 'asn_data': asn_data, 'cloaking_found': cloaking_found,
        'cloaking_results': cloaking_results, 'page_response': page_response,
        'phone_results': phone_results or None, 'spam_found': spam_found,
        'seized_domain': seized_domain, 'robots_disabled': robots_disabled, 'robots_txt': domain+'/robots.txt',
        'on_bad_pharma_list': on_bad_pharma_list, 'on_spam_list': on_spam_list,
        'third_party_base_href': third_party_base_href,
        'third_party_base_href_results': third_party_base_href_results,
        'redirect_results': redirect_results, 'redirect_loc': redirect_loc,
        'affiliates': affiliates, 'affiliate_details': affiliate_details
    }


def format_results_hacked_site(results, query):
    final_destination_url = ''
    affiliate_url = ''
    final_destination_domain = ''
    final_destination_ip = ''
    final_destination_asn = ''
    final_destination_as_country = ''
    final_destination_as_desc = ''
    final_destination_seized_domain = ''
    final_destination_on_bad_pharma_list = ''
    final_destination_contains_spam = ''
    final_destination_on_spam_list = ''
    final_destination_phone_results = ''
    final_destination_robots_disabled = ''
    affiliate_domain = ''
    affiliate_ip = ''
    affiliate_asn = ''
    affiliate_as_country = ''
    affiliate_as_desc = ''
    affiliate_on_spam_list = ''
    affiliate_robots_disabled = ''

    if results['third_party_base_href_results'] or results['redirect_results']:
        if results['third_party_base_href_results']:
            src = 'third_party_base_href_results'

        else:
            src = 'redirect_results'

            if results[src]['affiliate_details']:
                first_affiliate = next(iter(results[src]['affiliate_details'].values()))
                affiliate_url = first_affiliate['url']
                affiliate_domain = first_affiliate['domain']
                affiliate_ip = first_affiliate['ip']
                affiliate_asn = first_affiliate['asn_data']['as_number']
                affiliate_as_desc = first_affiliate['asn_data']['as_description']
                affiliate_as_country = first_affiliate['asn_data']['as_country_code']
                affiliate_on_spam_list = first_affiliate['on_spam_list']
                affiliate_robots_disabled = first_affiliate['robots_disabled']

        final_destination_url = results[src]['url']
        final_destination_domain = results[src]['domain']
        final_destination_ip = results[src]['ip']
        final_destination_asn = results[src]['asn_data']['as_number']
        final_destination_as_country = results[src]['asn_data']['as_country_code']
        final_destination_as_desc = results[src]['asn_data']['as_description']
        final_destination_contains_spam = results[src]['spam_found']
        final_destination_phone_results = results[src]['phone_results']
        final_destination_on_spam_list = results[src]['on_spam_list']
        final_destination_on_bad_pharma_list = results[src]['on_bad_pharma_list']
        final_destination_seized_domain = results[src]['seized_domain']
        final_destination_robots_disabled = results[src]['robots_disabled']

    key = hashlib.md5((results['url'] + final_destination_url).encode('utf-8')).hexdigest()

    df = pd.DataFrame([{
        'key': key,
        'entrance_domain': results['domain'],
        'google_query': query,
        'entrance_url': results['url'],
        'entrance_cloaking_found': results['cloaking_found'],
        'entrance_direct_version_contains_spam': results['spam_found'],
        'entrance_gogole_ref_version_contains_spam': contains_spam(results['cloaking_results'][('user', 'google-ref')]['response'].text),
        'entrance_redirect_url': results['redirect_loc'],
        'entrance_page_redirect_afiliate_url': affiliate_url,
        'entrance_page_third_party_frame_or_base_url': results['third_party_base_href'],
        'entrance_page_final_destination_url': final_destination_url,
        'entrance_domains_ip': results['ip'],
        'entrance_asn_country': results['asn_data']['as_country_code'],
        'entrance_domain_asn': results['asn_data']['as_number'],
        'entrance_domain_as_desc': results['asn_data']['as_description'],
        'final_destination_domain': final_destination_domain,
        'final_destination_ip': final_destination_ip,
        'final_destination_asn': final_destination_asn,
        'final_destination_as_country': final_destination_as_country,
        'final_destination_as_desc': final_destination_as_desc,
        'final_destination_contains_spam': final_destination_contains_spam,
        'final_destination_phone_results': final_destination_phone_results,
        'final_destination_on_spam_list': final_destination_on_spam_list,
        'final_destination_on_bad_pharma_list': final_destination_on_bad_pharma_list,
        'final_destination_seized_domain': final_destination_seized_domain,
        'final_destination_robots_disabled': final_destination_robots_disabled,
        'affiliate_domain': affiliate_domain,
        'affiliate_ip': affiliate_ip,
        'affiliate_asn': affiliate_asn,
        'affiliate_as_country': affiliate_as_country,
        'affiliate_as_desc': affiliate_as_desc,
        'affiliate_on_spam_list': affiliate_on_spam_list,
        'affiliate_robots_disabled': affiliate_robots_disabled,
        'timestamp': results['timestamp']
    }])
    df = df.set_index('key')
    return df


def write_result_to_file(df, log_dir=LOG_DIR):
    csv_loc = log_dir + 'hacked_sites.csv'
    try:
        existing_df = pd.read_csv(csv_loc, index_col='key')
        new_df = existing_df.append(df)
        new_df = new_df[~new_df.index.duplicated(keep='first')]
    except:
        pd.DataFrame()
        new_df = df
    new_df.to_csv(csv_loc, header=True,
                  columns=[
                            'google_query', 'entrance_domain', 'google_query',
                            'entrance_url', 'entrance_cloaking_found',
                            'entrance_direct_version_contains_spam', 'entrance_gogole_ref_version_contains_spam',
                            'entrance_redirect_url', 'entrance_page_redirect_afiliate_url',
                            'entrance_page_third_party_frame_or_base_url', 'entrance_page_final_destination_url',
                            'entrance_domains_ip', 'entrance_asn_country','entrance_domain_asn',
                            'entrance_domain_as_desc','final_destination_domain','final_destination_ip','final_destination_asn',
                            'final_destination_as_country','final_destination_as_desc','final_destination_contains_spam',
                            'final_destination_phone_results','final_destination_on_spam_list','final_destination_on_bad_pharma_list',
                            'final_destination_seized_domain','final_destination_robots_disabled','affiliate_domain',
                            'affiliate_ip','affiliate_asn','affiliate_as_country','affiliate_as_desc','affiliate_on_spam_list',
                            'affiliate_robots_disabled'
                  ])


def gather_intel_and_save(link, query):
    r = gather_intel_url(link, follow_links=True)
    print ("formatting results...")
    d = format_results_hacked_site(r, query=query)
    print ("writing results to file...")
    write_result_to_file(df=d)
    return r


def gather_intel_search(search_query):
    # time.sleep(5)
    pass

